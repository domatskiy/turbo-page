<?php

namespace Domatskiy\TurboPage\Page;

use Domatskiy\TurboPage\Page\Menu\Item;

/**
 * Class Menu
 * @package Domatskiy\TurboPage
 */
class Menu
{
    /**
     * @var Item[]
     */
    protected array $items = [];

    public function __construct()
    {
        //
    }

    public function addItem(Item $item):Menu
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * @return Item[]
     */
    public function getItems():array
    {
        return $this->items;
    }
}
