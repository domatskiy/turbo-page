# Yandex turbo pages

###Install

```bash
composer require domatskiy/turbo-page
```

###Usage

#####create page
```php

# create menu
$menu = new TurboPage\Page\Menu();
$menu
    ->addItem(new TurboPage\Page\Menu\Item('Ссылка 1', 'http://localhost/page'))
    ->addItem(new TurboPage\Page\Menu\Item('Ссылка 2', 'http://localhost/page2'))
    ->addItem(new TurboPage\Page\Menu\Item('Ссылка 3', 'http://localhost/page3'));
  
# create content
$content = new TurboPage\Page\Content();
$content
    ->addParagraph('hello world')
    ->addList([
        'item 1',
        'item 2',
        'item 3',
    ])
    ->addImage('http://localhost/image.png', 'image title')
    ->addParagraph('goodbuy');

# or set content
# $content->setContent('<p>hello world</p>');


# create page
$page = new TurboPage\Page('Test page', 'http://localhost/page');
$page
    ->setMenu($menu)
    ->setContent($content);
```
  
#####create chanel
```php
# create rss 
$rss = new TurboPage('my channel', 'https://example.com');

# add page to rss
$rss->addPage($page);

# get xml
$xml = $rss->getXML();
```
