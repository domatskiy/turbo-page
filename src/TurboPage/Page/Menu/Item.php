<?php

namespace Domatskiy\TurboPage\Page\Menu;

/**
 * Class Item
 */
class Item
{
    protected string $title;
    protected string $url;

    public function __construct(string $title, string $url)
    {
        $this->title = $title;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}
