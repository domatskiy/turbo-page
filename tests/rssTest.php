<?php

namespace Domatskiy\TurboPage\Test;

use Domatskiy\TurboPage;
use PHPUnit\Framework\TestCase;

class rssTest extends TestCase
{
    public function testXML()
    {
        $menu = new TurboPage\Page\Menu();
        $menu
            ->addItem(new TurboPage\Page\Menu\Item('Ссылка 1', 'http://localhost/page'))
            ->addItem(new TurboPage\Page\Menu\Item('Ссылка 2', 'http://localhost/page2'))
            ->addItem(new TurboPage\Page\Menu\Item('Ссылка 3', 'http://localhost/page3'));

        $content = new TurboPage\Page\Content();

        $content
            ->addParagraph('hello world')
            ->addList([
                'item 1',
                'item 2',
                'item 3',
            ])
            ->addImage('http://localhost/imahe.png', 'image title')
            ->addParagraph('goodbuy');

        $page = new TurboPage\Page('Test page', 'http://localhost/page');
        $page
            ->setMenu($menu)
            ->setContent($content);

        $rss = new TurboPage('my channel', 'https://example.com');
        $rss->addPage($page);
        $xml = $rss->getXML();

        $this->assertTrue(is_string($xml));
        echo $xml;
    }
}
