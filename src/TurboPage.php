<?php

namespace Domatskiy;

use Domatskiy\TurboPage\Page;
use SimpleXMLElement;

/**
 * Class TurboPage
 * @package Domatskiy
 */
class TurboPage
{
    protected string $channelTitle;
    protected string $channelUrl;

    /**
     * @var Page[]
     */
    protected array $pages = [];

    public function __construct(string $channelTitle, string $channelUrl)
    {
        $this->channelTitle = $channelTitle;
        $this->channelUrl = $channelUrl;
    }

    public function addPage(Page $contentPage):void
    {
        $this->pages[] = $contentPage;
    }

    /**
     * @return string
     */
    public function getXML(): string
    {
        $rss = new SimpleXMLElement('<rss/>');
        $rss->addAttribute('xmlns:yandex', 'http://news.yandex.ru');
        $rss->addAttribute('xmlns:media', 'http://search.yahoo.com/mrss/');
        $rss->addAttribute('xmlns:turbo', 'http://turbo.yandex.ru');
        $rss->addAttribute('version', '2.0');

        //$rss->registerXPathNamespace('', 'turbo');

        $channel = $rss->addChild('channel');
        $channel->addChild('title', $this->channelTitle);
        $channel->addChild('link', $this->channelUrl);
        // $channel->addChild('turbo:cms_plugin', $this->channel_url, 'turbo');

        foreach ($this->pages as $page) {
            /** @var Page $page */
            $item = $channel->addChild('item');
            $item->addAttribute('turbo', 'true');

            $item->addChild('link', $page->getUrl());
            $item->addChild('turbo:content', '<![CDATA['.$page->getHTML().']]>', 'turbo');
        }

        $xml = $rss->asXML();
        $xml = html_entity_decode($xml, ENT_NOQUOTES, 'UTF-8');
        return $xml;
    }

    public function xmlResponse(): void
    {
        Header('Content-type: text/xml');
        echo $this->getXML();
    }
}
