<?php

namespace Domatskiy\TurboPage;

use Domatskiy\TurboPage\Page\Content;
use Domatskiy\TurboPage\Page\Menu;
use Exception;

/**
 * Class Content
 * @package Domatskiy\TurboPage
 */
class Page
{
    protected ?string $url = null;

    protected ?string $headerTitle = null;

    protected ?string $headerImage = null;

    protected ?Content $content = null;

    /**
     * @var Menu\Item[]
     */
    protected array $menu = [];

    /**
     * @throws Exception
     */
    public function __construct(string $title, string $url, string $image = '')
    {
        if (!$url) {
            throw new Exception('empty link');
        } elseif (strlen($url) > 243) {
            throw new Exception('the maximum length of the link 243 ASCII character');
        } elseif (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new Exception('not a valid url');
        }

        if (!$title) {
            throw new Exception('empty page title (h1)');
        }

        if ($image && !filter_var($image, FILTER_VALIDATE_URL)) {
            throw new Exception('not a valid image url');
        }

        $this->url = $url;
        $this->headerTitle = $title;
        $this->headerImage = $image;
    }

    /**
     * @param Content $c
     * @return Page
     */
    public function setContent(Content $c):Page
    {
        $this->content = $c;

        return $this;
    }

    /**
     * Устанавливает меню
     *
     * @param Menu $menu
     * @return Page
     * @throws Exception
     */
    public function setMenu(Menu $menu):Page
    {
        $this->menu = $menu->getItems();

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getHTML(): string
    {
        $header = [
            '<header>',
            '<h1>'.$this->headerTitle.'</h1>'
        ];

        if ($this->headerImage) {
            $header[] = '<figure><img src="'.$this->headerImage.'" /></figure>';
        }

        if ($this->menu) {
            $header[] = '<menu>';
            foreach ($this->menu as $item) {
                $header[] = '<a href="'.$item->getUrl().'">'.$item->getTitle().'</a>';
            }
            $header[] = '</menu>';
        }

        $header[] = '</header>';

        return implode("\n", $header).implode("\n", $this->content->getContent());
    }
}
