<?php

namespace Domatskiy\TurboPage\Page;

use Exception;

class Content
{
    /**
     * @var string[]
     */
    protected array $content = [];

    /**
     * @param string $p
     * @return $this
     */
    public function addParagraph(string $p):Content
    {
        $this->content[] = '<p>'.$p.'</p>';

        return $this;
    }

    /**
     * @param array $list
     * @return $this
     * @throws Exception
     */
    public function addList(array $list = []):Content
    {
        if (empty($list)) {
            return $this;
        }
        $tmp = '<li>';

        foreach ($list as $item) {
            if (!is_string($item)) {
                throw new Exception('Not correct list item');
            }
            $tmp .= '<li>'.$item.'</li>';
        }

        $tmp .= '</li>';

        $this->content[] = $tmp;

        return $this;
    }

    /**
     * @param string $link
     * @param string $title
     * @return $this
     * @throws Exception
     */
    public function addImage(string $link, string $title = ''):Content
    {
        if (!$link) {
            throw new Exception('empty link');
        } elseif (!filter_var($link, FILTER_VALIDATE_URL)) {
            throw new Exception('not a valid url');
        }

        $tmp = '<figure>';
        $tmp .= '<img src="'.$link.'"/>';

        if ($title) {
            $tmp .= '<figcaption>'.strip_tags($title).'</figcaption>';
        }

        $tmp .= '</figure>';

        $this->content[] = $tmp;

        return $this;
    }

    /**
     * @param string $c
     * @return $this
     */
    public function setContent(string $c):Content
    {
        $this->content = [$c];

        return $this;
    }

    /**
     * @return string[]
     */
    public function getContent():array
    {
        return $this->content;
    }
}
